﻿using System;

namespace Models
{
	[Serializable]
	public class Score
	{
		public string username;

		public int score;

		public override string ToString(){
			return UnityEngine.JsonUtility.ToJson (this, true);
		}
	}

	[Serializable]
	public class PostResponse
	{
		public bool updated;

		public Score score;

		public int previousScore;

		public override string ToString(){
			return UnityEngine.JsonUtility.ToJson (this, true);
		}
	}

	[Serializable]
	public class DeleteResponse
	{
		public bool deleted;
		public string username;
	}

	[Serializable]
	public class LeaderboardResponse {
		public Score[] scores;
	}
}