﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using Proyecto26;
using Models;

public class Leaderboard : MonoBehaviour
{

    public TMP_InputField usernameInput;
    public TMP_InputField scoreInput;

    public TMP_Text LeaderboardText;

    private readonly string basePath = "http://localhost:3000";
    private RequestHelper currentRequest;

    public void POST()
    {
        Debug.Log("POST");
        currentRequest = new RequestHelper
        {
            Uri = basePath + "/scores",
            Body = new Score
            {
                username = usernameInput.text,
                score = int.Parse(scoreInput.text)
            }
        };

        RestClient.Post<PostResponse>(currentRequest)
        .Then(res =>
        {
            Debug.Log(JsonUtility.ToJson(res, true));
        })
        .Catch(err =>
        {
            Debug.LogError(err.Message);
        });
    }

    public void GET()
    {
        Debug.Log("GET");

		RestClient.Get<Score>(basePath + "/scores/" + usernameInput.text)
		.Then(res => {
			Debug.Log(JsonUtility.ToJson(res, true));
			scoreInput.text = res.score.ToString();
		})
        .Catch(err =>
        {
            Debug.LogError(err.Message);
        });

    }

    public void DELETE()
    {
        Debug.Log("DELETE");
		
		currentRequest = new RequestHelper
        {
            Uri = basePath + "/scores/" + usernameInput.text
        };
		RestClient.Delete(basePath + "/scores/" + usernameInput.text, (err, res) => {
			if(err != null){
				Debug.LogError(err.Message);
			}
			else{
				Debug.Log(JsonUtility.ToJson(res, true));
			}
		});
    }

    public void GETLEADERBOARD()
    {
        Debug.Log("LEADERBOARD");

		RestClient.Get<LeaderboardResponse>(basePath + "/scores")
		.Then(res => {
			Debug.Log(JsonUtility.ToJson(res, true));
			if (res.scores.Length > 0) {
				string leaderboard = "";
				foreach(Score score in res.scores) {
					leaderboard += score.username + ": " + score.score + "\n";
				}
				LeaderboardText.text = leaderboard;
			}
		})
        .Catch(err =>
        {
            Debug.LogError(err);
        });
    }
}
