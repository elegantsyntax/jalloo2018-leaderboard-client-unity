# Jalloo 2018 Leaderboard Client Unity

### Pre-requisites
- [Unity](https://store.unity.com/)
- [jalloo2018-leaderboard-server](https://gitlab.com/elegantsyntax/jalloo2018-leaderboard-server)

### Included Packages
- [RestClient for Unity](https://github.com/proyecto26/RestClient)
- [TextMesh Pro](https://assetstore.unity.com/packages/essentials/beta-projects/textmesh-pro-84126)
